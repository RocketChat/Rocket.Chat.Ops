![Rocket.Chat logo](https://rocket.chat/images/logo/logo-dark.svg?v3)

# Integrate with Rocket.Chat !

Integrate your apps with Rocket.Chat.   Powered by Hubot.

## Chat-ops is NOW CORE!

We are delighted to announce that Chat-ops is now part of core.  See [core repository](https://github.com/RocketChat/Rocket.Chat) for future development.

This repository will showcase a collection of Hubot and Meteor Packages that you can use in your own integrations!

## Hubots 

We have been testing our GitHub and GitLab integration hubots for months on our 
[community chat server](https://demo.rocket.chat/).

Meet *Baron Bogo*!   Our [GitHub integration hubot](https://github.com/RocketChat/Rocket.Chat.Ops/tree/develop/hubots/hubot-baronbogo).

Meet *Gitsy* !   Our [GitLab integration hubot](https://github.com/RocketChat/Rocket.Chat.Ops/tree/develop/hubots/hubot-gitsy).

Meet *Raffie* !  Our _baby_ AI frontier exploration learning hubot.  Backed by Alice on the mother side, and Watson on the other, will Raffie grow up to be genius that make his parents proud?   Stay tuned - coming soon.


## Meteor Packages 
* Drone Fleet Management ([mock proof-of-concept demo](https://github.com/RocketChat/Rocket.Chat.Ops/tree/develop/packages/rocketchat-chatops))


### Old Chat-ops information

100% powered by hubot.

Main features:

* flexible fly-in panels that can contain arbitrary user interface
* scalable inbound event handling
* scalable outbound command dispatching
* inbound data rendered by specialized widgets on fly-ins (example: code is rendered in syntax highlighed text editor)
* real time feeds, connected by chat fabric, p2p direct data stream from source <--> sinks into UI widget(s) on fly-ins
* modular, non-intrusive, integration with the mainstream Rocket.Chat core
* bots backwards compatible with Rocket.Chat core
* extension is per-room customizable, for example: one room for open source project Rocket.Chat developers via github integration, another for MineCraft server farms operators discussion and network monitoring, yet another for a drone delivery service's fleet monitoring and control (see screenshot below)

![Integrate your apps with fly-in panels](https://raw.githubusercontent.com/Sing-Li/bbug/master/images/dronechatops.png)

![Full syntax highlighted text editor on a fly-in](https://cloud.githubusercontent.com/assets/122633/9616075/2d6b419c-50ca-11e5-8eef-3d378250396d.png)



[![Test Coverage](https://codeclimate.com/github/RocketChat/Rocket.Chat.Ops/badges/coverage.svg)](https://codeclimate.com/github/RocketChat/Rocket.Chat.Ops/coverage)
[![Code Climate](https://codeclimate.com/github/RocketChat/Rocket.Chat.Ops/badges/gpa.svg)](https://codeclimate.com/github/RocketChat/Rocket.Chat.Ops)
[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://github.com/RocketChat/Rocket.Chat/raw/master/LICENSE)

